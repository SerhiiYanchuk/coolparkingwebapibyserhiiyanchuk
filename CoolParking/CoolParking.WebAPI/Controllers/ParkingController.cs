﻿using CoolParking.BL.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ParkingController : ControllerBase
    {
        private IParkingService parkingService;
        public ParkingController(IParkingService parkingService)
        {
            this.parkingService = parkingService;
        }

        [HttpGet("balance")]
        public ActionResult<decimal> GetBalance()
        {
            decimal balance = parkingService.GetBalance();
            return Ok(balance);
        }
        [HttpGet("capacity")]
        public ActionResult<int> GetCapacity()
        {
            int capacity = parkingService.GetCapacity();
            return Ok(capacity);
        }
        [HttpGet("freePlaces")]
        public ActionResult<int> GetFreePlaces()
        {
            int freePlaces = parkingService.GetFreePlaces();
            return Ok(freePlaces);
        }
    }
}
