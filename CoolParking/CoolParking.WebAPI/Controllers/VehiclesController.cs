﻿using AutoMapper;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.WebAPI.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;


namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class VehiclesController : ControllerBase
    {
        private readonly IParkingService parkingService;
        private readonly IMapper mapper;
        public VehiclesController(IParkingService parkingService, IMapper mapper)
        {
            this.parkingService = parkingService;
            this.mapper = mapper;
        }
        private bool IsIdValid(string id) => Regex.IsMatch(id, @"^[A-Z]{2}-[0-9]{4}-[A-Z]{2}$", RegexOptions.Compiled);

        [HttpGet]
        public ActionResult<IEnumerable<VehicleModel>> GetVehicles()
        {
            ReadOnlyCollection<Vehicle> vehicles = parkingService.GetVehicles();
            return Ok(mapper.Map<ReadOnlyCollection<Vehicle>, IEnumerable<VehicleModel>>(vehicles));
        }

        [HttpGet("{id}")]
        public ActionResult<VehicleModel> GetVehicle(string id)
        {
            if(!IsIdValid(id))
                return BadRequest("Wrong format. Format ID is XX-YYYY-XX (where X is any uppercase letter of the English alphabet and Y is any number)");
            
            Vehicle vehicle = parkingService.FindVehicleById(id);
            if (vehicle is null)
                return NotFound($"Vehicle with {id} doesn't exist");

            return Ok(mapper.Map<VehicleModel>(vehicle));
        }

        [HttpPost]
        public ActionResult<VehicleModel> PostVehicle(VehicleModel vehicle)
        {
            (string id, VehicleTypeModel vehicleType, decimal balance) = vehicle;
            try
            {
                Vehicle newVehicle = new Vehicle(id, mapper.Map<VehicleType>(vehicleType), balance);
                parkingService.AddVehicle(newVehicle);
                return Created("/api/vehicles/" + newVehicle.Id, mapper.Map<VehicleModel>(newVehicle));
            }
            catch(ArgumentException e)
            {
                return BadRequest(e.Message);
            }
            catch(InvalidOperationException e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpDelete("{id}")]
        public ActionResult DeleteVehicle(string id)
        {
            if (!IsIdValid(id))
                return BadRequest("Wrong format. Format ID XX-YYYY-XX (where X is any uppercase letter of the English alphabet and Y is any number)");

            try
            {
                parkingService.RemoveVehicle(id);
                return NoContent();
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }
            catch (InvalidOperationException e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
