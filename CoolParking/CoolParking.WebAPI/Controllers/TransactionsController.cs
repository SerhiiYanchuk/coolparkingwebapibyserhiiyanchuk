﻿using AutoMapper;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.WebAPI.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TransactionsController : ControllerBase
    {
        private readonly IParkingService parkingService;
        private readonly IMapper mapper;
        public TransactionsController(IParkingService parkingService, IMapper mapper)
        {
            this.parkingService = parkingService;
            this.mapper = mapper;
        }
        private bool IsIdValid(string id) => Regex.IsMatch(id, @"^[A-Z]{2}-[0-9]{4}-[A-Z]{2}$", RegexOptions.Compiled);

        [HttpGet("last")]
        public ActionResult<IEnumerable<TransactionModel>> GetLast()
        {
            TransactionInfo[] transactions = parkingService.GetLastParkingTransactions();
            return Ok(mapper.Map<TransactionInfo[], IEnumerable<TransactionModel>>(transactions));
        }

        [HttpGet("all")]
        public ActionResult<string> GetAll()
        {          
            try
            {
                string transactions = parkingService.ReadFromLog();
                return Ok(transactions);
            }
            catch(InvalidOperationException e)
            {
                return NotFound(e.Message);
            }
            
        }
        [HttpPut("topUpVehicle")]
        public ActionResult<VehicleModel> GetAll(TopUpTransaction topUpInfo)
        {           
            if (!topUpInfo.IsValid(out string errors))
                return BadRequest(errors);

            (string id, decimal sum) = topUpInfo;
            try
            {
                Vehicle vehicle = parkingService.TopUpVehicle(id, sum);
                return Ok(mapper.Map<VehicleModel>(vehicle));
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }            
        }
    }
}
