﻿using System;

namespace CoolParking.WebAPI.Models
{
    public struct TransactionModel
    {
        public string vehicleId { get; set; }
        public decimal sum { get; set; }
        public DateTime transactionDate { get; set; }
    }
}
