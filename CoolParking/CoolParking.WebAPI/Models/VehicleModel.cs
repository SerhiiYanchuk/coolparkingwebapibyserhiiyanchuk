﻿

namespace CoolParking.WebAPI.Models
{
    public class VehicleModel
    {      
        public string id { get; set; }
        public VehicleTypeModel vehicleType { get; set; }
        public decimal balance { get; set; }
        public void Deconstruct(out string id, out VehicleTypeModel vehicleType, out decimal balance)
        {
            id = this.id;
            vehicleType = this.vehicleType;
            balance = this.balance;
        }
    }
}
