﻿using System.Text.RegularExpressions;

namespace CoolParking.WebAPI.Models
{
    public class TopUpTransaction
    {        
        public string id { get; set; }
        public decimal sum { get; set; }
        public void Deconstruct(out string id, out decimal sum)
        {
            id = this.id;
            sum = this.sum;
        }
        public bool IsValid(out string errors)
        {
            errors = string.Empty;
            if (!Regex.IsMatch(id, @"^[A-Z]{2}-[0-9]{4}-[A-Z]{2}$", RegexOptions.Compiled))
                errors += "Wrong format. Format ID XX-YYYY-XX (where X is any uppercase letter of the English alphabet and Y is any number)\n";
            if (sum < 0)
                errors += "The top-up amount cannot be negative\n";
            if (errors == string.Empty)
                return true;
            return false;
        }
    }
}
