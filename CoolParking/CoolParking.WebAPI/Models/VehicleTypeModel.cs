﻿
namespace CoolParking.WebAPI.Models
{
    public enum VehicleTypeModel : byte
    {
        Undefined = 0,
        PassengerCar,
        Truck,
        Bus,
        Motorcycle
    }
}
