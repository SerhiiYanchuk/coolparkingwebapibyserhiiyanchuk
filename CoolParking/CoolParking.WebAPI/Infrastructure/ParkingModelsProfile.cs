﻿using AutoMapper;
using CoolParking.BL.Models;
using CoolParking.WebAPI.Models;

namespace CoolParking.WebAPI.Infrastructure
{
    public class ParkingModelsProfile : Profile
    {
        public ParkingModelsProfile()
        {
            CreateMap<VehicleType, VehicleTypeModel>().ReverseMap();
            CreateMap<Vehicle, VehicleModel>();
            CreateMap<TransactionInfo, TransactionModel>().ForMember(p => p.vehicleId, p => p.MapFrom(t => t.TransportId))
                                                          .ForMember(p => p.sum, p => p.MapFrom(t => t.Sum))
                                                          .ForMember(p => p.transactionDate, p => p.MapFrom(t => t.WithdrawTime));
        }
    }
}