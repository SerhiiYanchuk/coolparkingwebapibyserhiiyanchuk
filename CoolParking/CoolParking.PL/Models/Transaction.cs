﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoolParking.PL.Models
{
    public struct Transaction
    {
        public string vehicleId { get; set; }
        public decimal sum { get; set; }
        public DateTime transactionDate { get; set; }

        public override string ToString() => $"Transport ID: {vehicleId}  payment: {sum,-4}  time: {transactionDate}";
    }
}
