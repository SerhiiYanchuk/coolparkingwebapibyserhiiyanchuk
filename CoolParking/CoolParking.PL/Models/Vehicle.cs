﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CoolParking.PL.Models
{
    public class Vehicle
    {
        public string id { get; set; }
        public VehicleType vehicleType { get; set; }
        public decimal balance { get; set; }
        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            this.id = id;
            this.vehicleType = vehicleType;
            this.balance = balance;
        }
        public override string ToString() => $"ID: {id}  type: {vehicleType,-12}  balance: {balance}";
    }
}
