﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CoolParking.PL.Models
{
    public class TopUpInfo
    {
        public string id { get; set; }
        public decimal sum { get; set; }
        public TopUpInfo(string id, decimal sum)
        {
            this.id = id;
            this.sum = sum;
        }
    }
}
