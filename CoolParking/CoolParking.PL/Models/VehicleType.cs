﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoolParking.PL.Models
{
    public enum VehicleType : byte
    {
        Undefined = 0,
        PassengerCar,
        Truck,
        Bus,
        Motorcycle
    }
}
