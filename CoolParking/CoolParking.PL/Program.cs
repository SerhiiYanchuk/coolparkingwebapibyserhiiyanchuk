﻿using CoolParking.PL.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Reflection;

namespace CoolParking.PL
{
    class Program
    {
        static void Main(string[] args)
        {
            string apiUrl = "https://localhost:44352/api";
            foreach (var vehicle in VehicleFactory.GenerateVehicles(5))
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    var response = client.PostAsJsonAsync<Vehicle>($"{apiUrl}/vehicles", vehicle).Result;
                }
            }
            
            while (true)
            {
                Console.Clear();
                Console.WriteLine("-----------------------");
                Console.WriteLine($"{' ',-3}CoolParking\n{' ',-3}Main menu");
                Console.WriteLine("-----------------------\n");
                Console.WriteLine("1 - Parking information");
                Console.WriteLine("2 - Vehicle management");

                Console.WriteLine("0 - Exit");
                if (int.TryParse(Console.ReadLine(), out int choice))
                {
                    switch (choice)
                    {
                        case 1:
                            ParkingInfoSubMenu(apiUrl);
                            break;
                        case 2:
                            VehicleManagementSubMenu(apiUrl);
                            break;
                        case 0:
                            return;
                        default:
                            Console.WriteLine("Incorrect input. Try again. I believe in you :)");
                            break;
                    }
                }
            }


        }
        static void ParkingInfoSubMenu(string apiUrl)
        {
            while (true)
            {
                Console.Clear();
                Console.WriteLine("----------------------");
                Console.WriteLine($"{' ',-3}CoolParking\n{' ',-3}Parking information");
                Console.WriteLine("----------------------");
                Console.WriteLine("1 - Display the current balance of the Parking");
                Console.WriteLine("2 - Display the capacity of parking spots");
                Console.WriteLine("3 - Display the number of free parking spots");
                Console.WriteLine("4 - Display all parking transactions for the current period (before being logged)");
                Console.WriteLine("5 - Display the transaction history (by reading data from the transactions.log file)\n");
                Console.WriteLine("0 - Back");
                Console.Write("> ");
                if (int.TryParse(Console.ReadLine(), out int choice))
                {
                    switch (choice)
                    {
                        case 1:
                            {
                                using (var client = new HttpClient())
                                {
                                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("text/plain"));
                                    var response = client.GetAsync($"{apiUrl}/parking/balance").Result;
                                    if (response.IsSuccessStatusCode)
                                    {
                                        //decimal balance = JsonConvert.DeserializeObject<Vehicle>(response.Content.ReadAsStringAsync().Result);
                                        decimal balance = decimal.Parse(response.Content.ReadAsStringAsync().Result);
                                        Console.WriteLine($"Current balance: {balance}");
                                    }
                                    else
                                    {
                                        Console.WriteLine("Error");
                                        Console.WriteLine($"Status: {response.StatusCode}");
                                        Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                                    }
                                }
                                break;
                            }
                        case 2:
                            {
                                using (var client = new HttpClient())
                                {
                                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("text/plain"));
                                    var response = client.GetAsync($"{apiUrl}/parking/capacity").Result;
                                    if (response.IsSuccessStatusCode)
                                    {
                                        //decimal balance = JsonConvert.DeserializeObject<Vehicle>(response.Content.ReadAsStringAsync().Result);
                                        int capacity = int.Parse(response.Content.ReadAsStringAsync().Result);
                                        Console.WriteLine($"Capacity of parking spots: {capacity}");
                                    }
                                    else
                                    {
                                        Console.WriteLine("Error");
                                        Console.WriteLine($"Status: {response.StatusCode}");
                                        Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                                    }
                                }
                                break;
                            }
                        case 3:
                            {
                                using (var client = new HttpClient())
                                {
                                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("text/plain"));
                                    var response = client.GetAsync($"{apiUrl}/parking/freePlaces").Result;
                                    if (response.IsSuccessStatusCode)
                                    {
                                        //decimal balance = JsonConvert.DeserializeObject<Vehicle>(response.Content.ReadAsStringAsync().Result);
                                        int freePlaces = int.Parse(response.Content.ReadAsStringAsync().Result);
                                        Console.WriteLine($"Free spots: {freePlaces}");
                                    }
                                    else
                                    {
                                        Console.WriteLine("Error");
                                        Console.WriteLine($"Status: {response.StatusCode}");
                                        Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                                    }
                                }
                                break;
                            }
                        case 4:
                            {
                                using (var client = new HttpClient())
                                {
                                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                                    var response = client.GetAsync($"{apiUrl}/transactions/last").Result;
                                    if (response.IsSuccessStatusCode)
                                    {
                                        IEnumerable<Transaction> transactions = JsonConvert.DeserializeObject<IEnumerable<Transaction>>(response.Content.ReadAsStringAsync().Result);
                                        foreach (var transaction in transactions)
                                            Console.WriteLine(transaction);
                                    }
                                    else
                                    {
                                        Console.WriteLine("Error");
                                        Console.WriteLine($"Status: {response.StatusCode}");
                                        Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                                    }
                                }
                                break;
                            }
                        case 5:
                            {
                                using (var client = new HttpClient())
                                {
                                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("text/plain"));
                                    var response = client.GetAsync($"{apiUrl}/transactions/all").Result;
                                    if (response.IsSuccessStatusCode)
                                    {
                                        string transactions = response.Content.ReadAsStringAsync().Result;
                                        Console.WriteLine($"The transaction history:\n\n{transactions}");
                                    }
                                    else
                                    {
                                        Console.WriteLine("Error");
                                        Console.WriteLine($"Status: {response.StatusCode}");
                                        Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                                    }
                                }
                                break;
                            }
                        case 0:
                            return;
                        default:
                            Console.WriteLine("Incorrect input. Try again. I believe in you :)");
                            break;
                    }
                    Console.WriteLine("\nPress any key");
                    Console.ReadKey();
                }
            }
        }
        static void VehicleManagementSubMenu(string apiUrl)
        {
            while (true)
            {
                Console.Clear();
                Console.WriteLine("-----------------------");
                Console.WriteLine($"{' ',-3}CoolParking\n{' ',-3}Management");
                Console.WriteLine("----------------------");
                Console.WriteLine("1 - Put a vehicle in the parking");
                Console.WriteLine("2 - Pick up a vehicle from the parking");
                Console.WriteLine("3 - Top up the balance of a vehicle");
                Console.WriteLine("4 - Display a vehicle by ID");
                Console.WriteLine("5 - Display the list of vehicles in the parking\n");
                Console.WriteLine("0 - Back");
                Console.Write("> ");
                if (int.TryParse(Console.ReadLine(), out int choice))
                {
                    switch (choice)
                    {
                        case 1:
                            {
                                Vehicle vehicle = VehicleFactory.CreateVehicle();
                                using (var client = new HttpClient())
                                {
                                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                                    var response = client.PostAsJsonAsync<Vehicle>($"{apiUrl}/vehicles", vehicle).Result;
                                    if (response.IsSuccessStatusCode)
                                    {
                                        Vehicle serializedVehicle = JsonConvert.DeserializeObject<Vehicle>(response.Content.ReadAsStringAsync().Result);
                                        Console.WriteLine($"The vehicle has been putted in the parking\n{serializedVehicle}");
                                    }
                                    else
                                    {
                                        Console.WriteLine("Error");
                                        Console.WriteLine($"Status: {response.StatusCode}");
                                        Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                                    }
                                }
                                break;
                            }
                           
                        case 2:
                            {                           
                                using (var client = new HttpClient())
                                {
                                    Console.Write("Transport plate number (YY-XXXX-YY) > ");
                                    string id = Console.ReadLine();
                                    var response = client.DeleteAsync($"{apiUrl}/vehicles/{id}").Result;
                                    if (response.IsSuccessStatusCode)
                                        Console.WriteLine("The vehicle has been removed from the parking");
                                    else
                                    {
                                        Console.WriteLine("Error");
                                        Console.WriteLine($"Status: {response.StatusCode}");
                                        Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                                    }
                                }
                                break;
                            }
                            
                        case 3:
                            {
                                using (var client = new HttpClient())
                                {
                                    Console.Write("Transport plate number (YY-XXXX-YY) > ");
                                    string id = Console.ReadLine();
                                    decimal amount;
                                    while (true)
                                    {
                                        Console.Write("Top-up amount > ");
                                        if (decimal.TryParse(Console.ReadLine(), out amount))
                                            break;
                                    }
                                    TopUpInfo info = new TopUpInfo(id, amount);
                                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                                    var response = client.PutAsJsonAsync<TopUpInfo>($"{apiUrl}/transactions/topUpVehicle", info).Result;
                                    if (response.IsSuccessStatusCode)
                                    {
                                        Vehicle serializedVehicle = JsonConvert.DeserializeObject<Vehicle>(response.Content.ReadAsStringAsync().Result);
                                        Console.WriteLine($"The balance has been topped up\n{serializedVehicle}");
                                    }                                      
                                    else
                                    {
                                        Console.WriteLine("Error");
                                        Console.WriteLine($"Status: {response.StatusCode}");
                                        Console.WriteLine(response.Content.ReadAsStringAsync().Result.ToString());
                                    }
                                }
                                break;
                            }
                        case 4:
                            {
                                using (var client = new HttpClient())
                                {
                                    Console.Write("Transport plate number (YY-XXXX-YY) > ");
                                    string id = Console.ReadLine();

                                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                                    var response = client.GetAsync($"{apiUrl}/vehicles/{id}").Result;
                                    if (response.IsSuccessStatusCode)
                                    {
                                        Vehicle serializedVehicle = JsonConvert.DeserializeObject<Vehicle>(response.Content.ReadAsStringAsync().Result);
                                        Console.WriteLine(serializedVehicle);
                                    }
                                    else
                                    {
                                        Console.WriteLine("Error");
                                        Console.WriteLine($"Status: {response.StatusCode}");
                                        Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                                    }
                                }
                                break;
                            }
                        case 5:
                            {
                                using (var client = new HttpClient())
                                {
                                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                                    var response = client.GetAsync($"{apiUrl}/vehicles").Result;
                                    if (response.IsSuccessStatusCode)
                                    {
                                        IEnumerable<Vehicle> serializedVehicles = JsonConvert.DeserializeObject<IEnumerable<Vehicle>>(response.Content.ReadAsStringAsync().Result);
                                        foreach(var vehicle in serializedVehicles)
                                            Console.WriteLine(vehicle);
                                    }
                                    else
                                    {
                                        Console.WriteLine("Error");
                                        Console.WriteLine($"Status: {response.StatusCode}");
                                        Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                                    }
                                }
                                break;
                            }
                        case 0:
                            return;
                        default:
                            Console.WriteLine("Incorrect input. Try again. I believe in you :)");
                            break;
                    }
                    Console.WriteLine("\nPress any key");
                    Console.ReadKey();
                }
            }
        }
    }
}
